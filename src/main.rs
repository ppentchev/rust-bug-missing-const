/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Demonstrate a false `clippy::missing_const_for_fn` positive.

#![deny(missing_docs)]
// Activate most of the clippy::restriction lints that we've encountered...
#![warn(clippy::pattern_type_mismatch)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::print_stdout)]
// ...except for these ones.
#![allow(clippy::implicit_return)]
// Activate some of the clippy::nursery lints...
#![warn(clippy::missing_const_for_fn)]

use std::env;

/// Runtime configuration for the bug-missing-const tool.
#[derive(Debug, PartialEq)]
#[non_exhaustive]
struct Config {
    /// The current program name.
    program: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            program: "(unspecified)".to_owned(),
        }
    }
}

impl Config {
    /// Specify the program name.
    fn with_program(self, program: String) -> Self {
        Self { program, ..self }
    }
}

/// Parse the command-line arguments into a configuration structure.
fn parse_args() -> Config {
    let args: Vec<String> = env::args().collect();
    match *args {
        [] => expect_exit::exit("Not even a program name?"),
        [ref program] => Config::default().with_program(program.clone()),
        _ => expect_exit::exit("Usage: bug-missing-const"),
    }
}

#[cfg(test)]
mod tests {
    use super::Config;

    #[test]
    fn test_config_default() {
        assert!(
            Config::default()
                == Config {
                    program: "(unspecified)".to_owned()
                }
        );
    }

    #[test]
    fn test_config_with_program() {
        assert!(
            Config::default().with_program("616".to_owned())
                == Config {
                    program: "616".to_owned()
                }
        );
    }
}

#[allow(clippy::print_stdout)]
fn main() {
    let cfg = parse_args();
    println!("Program name: {}", cfg.program);
}
